# Projet Movie explorer du groupe 1 réalisé dans le cadre de l'UE 3DW24.

# movie-explorer

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.15.1.

## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.
