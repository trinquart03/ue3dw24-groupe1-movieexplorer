'use strict';

/**
 * @ngdoc overview
 * @name movieExplorerApp
 * @description
 * # movieExplorerApp
 *
 * Main module of the application.
 */
angular
  .module('movieExplorerApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'Main2Ctrl',
        controllerAs: 'main2'
      })
      .when('/movie/:movieId', {
        templateUrl: 'views/movie.html',
        controller: 'MovieCtrl',
        controllerAs: 'movieCtrl'
      })
	  .when('/films', {
        templateUrl: 'views/films.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
	  .when('/sorties', {
        templateUrl: 'views/sorties.html',
        controller: 'SortiesCtrl',
        controllerAs: 'sorties'
      })
	  .when('/acteurs', {
        templateUrl: 'views/acteurs.html',
        controller: 'ActeursCtrl',
        controllerAs: 'acteurs'
      })
	  .when('/actualites', {
        templateUrl: 'views/actualites.html',
        controller: 'ActualitesCtrl',
        controllerAs: 'actualites'
      })
	  .when('/contact', {
        templateUrl: 'views/contact.html',
        controller: 'ContactCtrl',
        controllerAs: 'contact'
      })
      .otherwise({
        redirectTo: '/'
      });

  });